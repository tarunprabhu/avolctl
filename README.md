# volumectl

`volumectl` is a wrapper script to control the volume level of the default 
output device. 
This relies on utilities exposed by the underlying sound system such as 
pactl or amixer. Although it can be used on the terminal, it is primarily
intended to be bound to multimedia keys or keyboard shortcuts. This will
obviously be slightly slower than calling the underlying mixer utility directly, 
but it shouldn't be noticeable on most modern systems. This is likely to be most
useful to those  who have to work across several different systems which 
have slightly different software setups with minimal desktop environments like 
Openbox that do not handle controlling the system automatically. 

# Requirements

`volumectl` is written in Lua and requires a lua interpreter and some Lua 
packages. 

## Interpreter

Lua >= 5.2

## Packages

These may need to be installed using [LuaRocks](https://luarocks.org). Your 
distribution may also provide one or more of these in the system repositories.

- [luaposix](https://luarocks.org/modules/gvvaughan/luaposix)
- [argparse](https://luarocks.org/modules/argparse/argparse)


# Installation

You can download just the `volumectl` script and copy it to some directory 
that is on your $PATH variable. 

# Usage

Run the following command on the terminal to see the list of commands and 
options available

```
volumectl -h
```
